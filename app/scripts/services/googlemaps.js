'use strict';

/**
 * @ngdoc service
 * @name frontfoodtruckApp.googleMaps
 * @description
 * # googleMaps
 * Factory in the frontfoodtruckApp.
 */
angular.module('frontfoodtruckApp')
  .factory('googleMaps', function ($http) {
    
    var googleMapService = [];

    var locations = [];

    // Coordenadas del Tec
    var selectedLat = 25.65329;
    var selectedLong = -100.29362;

    googleMapService = function(latitude, longitude) {

      locations = [];

      selectedLat = latitude;
      selectedLong = longitude;

      // Obtiene locaciones...
      /*
      $http.get('/').success(function(response) {
          
          locations = convertToMapPoints(response)
      }).error(function(){})
      */

      var convertToMapPoints = function(response) {

        var locations = [];

        for(var i = 0; response.lenght; i++) {

          var truck = response[i];

          var contentString = 
          '<p><b>Nombre:</b>' + truck.name +
          '<br><b>Tipo de Comida: </b>' + truck.kindOfFood +
          '<br><b>Telefono: </b>' + truck.phone +
          '</p>';

          locations.push({
            latlon: new google.maps.LatLng(truck.location[0], truck.location[1]),
            message: new google.maps.InfoWindow({
              content: contentString,
              maxWidth: 320
            }),
            name: truck.name,
            kindOfFood: truck.kindOfFood,
            phone: truck.phone
          });
        }

        return locations;
      };

    var initialize = function(latitude, longitude) {
      var latlng = {lat: selectedLat, selectedLong};

      if (!map) {
        var map = new google.maps.Map(document.getElementById('map'), {
          zom: 5,
          center: latlng
        });
      }

      locations.forEach(function(n, i) {
        var marker = new google.maps.Marker({
          position: m.latlon,
          map: map,
          title: "FoodTruck Map",
          icon: "https://d30y9cdsu7xlg0.cloudfront.net/png/745-200.png"
        });

        google.maps.event.addListener(marker, 'click', function(e) {

          currentSelectedMarker = n;
          n.message.open(map, marker);
        });
      });

      var initialLocation = new google.maps.LatLng(latitude, longitude);
      var marker = new google.maps.Marker({
        position: initialLocation,
        animation: google.maps.Animation.BOUNCE,
        map: map,
        icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
      });

      lastMarker = marker;
    };

    google.maps.event.addDomListener(window, 'load',
      googleMapService.refresh(selectedLat, selectedLong));

    }
  });
