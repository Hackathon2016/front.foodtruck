'use strict';

/**
 * @ngdoc service
 * @name frontfoodtruckApp.services
 * @description
 * # services
 * Factory in the frontfoodtruckApp.
 */
angular.module('frontfoodtruckApp')
    .factory('services', function($http, url) {

        var services = {
            api: api,
            getUser: getUser,
            getUserId: getUserId,
            getFoodTrucks: getFoodTrucks,
            getFoodTruckId: getFoodTruckId
        };

        return services;

        //////////////////

        function api() {
            return $http.get(url.api)
        }

        function getUser() {
            return $http.get(url.users);
        }

        function getUserId(id) {
            return $http.get(url.userId.replace(':id', id));
        }

        function getFoodTrucks(params) {
            var data = $.param({
                lat: params.lat,
                long: params.long,
                radius: params.radius
            });

            return $http.post(url.foodTrucks, data, { 'Content-Type': 'application/x-www-form-urlencoded' })
                .then(function(res) {
                    return res;
                });
        }

        function getFoodTruckId(id) {
            return $http.get(url.foodTruckId.replace(':id', id));
        }
    });
