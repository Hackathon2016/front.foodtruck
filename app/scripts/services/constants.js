'use strict';

/**
 * @ngdoc service
 * @name frontfoodtruckApp.constants
 * @description
 * # constants
 * Constant in the frontfoodtruckApp.
 */
angular.module('frontfoodtruckApp')
    .constant('url', {
    	'api': 'http://52.36.101.68/food-truck-api/',
    	'users': 'http://52.36.101.68/food-truck-api/index.php/users/',
    	'userId': 'http://52.36.101.68/food-truck-api/index.php/users/:id/',
    	'foodTrucks':  'http://52.36.101.68/food-truck-api/index.php/foodtrucks/nearby/',
    	'foodTruckId': 'http://52.36.101.68/food-truck-api/index.php/foodtrucks/:id'
    });
