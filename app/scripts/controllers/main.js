'use strict';

/**
 * @ngdoc function
 * @name frontfoodtruckApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontfoodtruckApp
 */
angular.module('frontfoodtruckApp')
    .controller('MainCtrl', function($scope, services) {
        var main = this;

        $scope.options = {
        	scrollwheel: false
        };

        $scope.user = [];
        $scope.foodTrucks = [];

        services.getUserId(getRandomUser())
            .then(function(res) {
                // TODO Isma mapear el usuario random
                //var lat = res.data.lat;
                //var lng = res.data.long;

                var lat = 25.65329;
                var lng = -100.29362;

                var coor = {
                	lat: parseFloat(lat),
					long: parseFloat(lng),
					radius: 10
                };
                //$scope.user = mapPoints(lat, lng, 'id');
                $scope.map = {
		        	center: {
		        		latitude: lat, 
		        		longitude: lng 
		        	},
		        	zoom: 12
		        };

		        $scope.marker = {
		        	id: 0,
		        	coords: {
		        		latitude: lat,
		        		longitude: lng
		        	},
		        	options: {
		        		draggable: false
		        	},
                    events: {
                        click: function(marker, eventName, args){
                            console.log('u suck mate');
                        }
                    }
		        };
		        
                //console.log($scope.user);
                services.getFoodTrucks(coor)
                	.then(function (res) {
                		console.log('getFoodTrucks', res)
                		// TODO mapping food trucks
                        var trucks = res.data.trucks;
                        var markers = [];
                        var cont = 0;
                        trucks.forEach(function(item) {
                            cont++;
                            markers.push(mapPoints(item.lat, item.long, 'id', cont))
                        });
                        $scope.foodTrucks = markers;
                        //$scope.foodTrucks.push(mapPoints(trucks.lat, trucks.long, 'id'));
                	});
            });

        function getRandomUser() {
            return Math.floor(Math.random() * (100 - 1) + 1);
        }

        function mapPoints(latitude, longitude, keyId, i) {
        	var ret = {
        		latitude: latitude,
        		longitude: longitude,
        		title: 'n' + i
        	}
        	ret[keyId] = i;
        	return ret;
        }
    });

	/*
        var setFoodTruckMarkers =  function(i, bounds, idKey) {
        	var lat_min   = bounds.southwest.latitude,
        		lat_range = bounds.northeast.latitude - lat_min,
        		lng_min   = bounds.southwest.longitude,
        		lng_range = bounds.northeast.longitude - lng_min;

        	if (idKey === null) {
        		idKey = 'id';
        	}

        	var latitude 	= lat_min + (Math.random() * lat_range);
        	var longitude 	= lng_min + (Math.random() * lng_range);

        	var ret = {
        		latitude: latitude,
        		longitude: longitude,
        		title: 'm' + i
        	};
        	ret[idKey] = i;
        	return ret;
        };

        $scope.randomMarkers = [];

        $scope.$watch(function() {
        	console.log($scope.map.bounds);
        	return $scope.map.bounds;
        }, function(nv, ov) {
	      if (!ov.southwest && nv.southwest) {
	        var markers = [];
	        for (var i = 0; i < 10; i++) {
	          markers.push(setFoodTruckMarkers(i, $scope.map.bounds, 'id'));
	        }
	        $scope.randomMarkers = markers;
	        console.log($scope.randomMarkers);
	      }
		}, true);
    });
>>>>>>> trucks location*/
