'use strict';

/**
 * @ngdoc function
 * @name frontfoodtruckApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontfoodtruckApp
 */
angular.module('frontfoodtruckApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
